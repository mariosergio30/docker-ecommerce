FROM openjdk:8-jdk-alpine
MAINTAINER mario.com
WORKDIR /ecommerce
COPY . .

RUN pwd
RUN ls -la

ENTRYPOINT ["java","-jar","micro-api-ecommerce-cloud.jar"]